import { ToastOptions } from "react-toastify";

export const DateFormat = 'dd MMM yyyy';

export const ToastSetting: ToastOptions = {
    position: "top-right",
    autoClose: 2500,
    hideProgressBar: false,
    closeOnClick: false,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
}