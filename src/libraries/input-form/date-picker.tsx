import clsx from 'clsx';
import { DateFormat } from 'constants/common';
import { ErrorMessage } from 'formik';
import { Fragment } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './styles.module.scss';

interface Props {
    field: any;
    form: any;

    className?: any;
    label?: string;
    required?: boolean;
}

export default function DatePickerForm(props: Props) {
    const { field, className, label, form } = props;
    const { name, value, onBlur } = field;
    const { errors, touched, setFieldValue } = form;

    // have error format
    const showError = errors[name] && touched[name];

    // class custom
    const classDatePicker = clsx(styles.datePicker, {
        className: className,
        [styles.showError]: showError
    });

    return (
        <Fragment>
            {label && (
                <div className={styles.labelView}>
                    <label className={styles.label} htmlFor={name}>
                        {label}
                    </label>
                </div>
            )}
            <div className={classDatePicker}>
                <DatePicker
                    className={styles.dateView}
                    id={name}
                    selected={value}
                    onChange={(value) => setFieldValue(name, value)}
                    onBlur={onBlur}
                    minDate={new Date()}
                    dateFormat={DateFormat}
                />
                <label className={styles.iconCalender} htmlFor={name}>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                        />
                    </svg>
                </label>
            </div>
            <div className={styles.errorMess}>
                <ErrorMessage name={name} />
            </div>
        </Fragment>
    );
}
