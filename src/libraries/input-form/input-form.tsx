import clsx from 'clsx';
import { ErrorMessage } from 'formik';
import { Fragment } from 'react';
import styles from './styles.module.scss';

interface Props {
    field: any;
    form: any;

    type?: any;
    placeholder?: string;
    className?: any;
    label?: string;
    required?: boolean
}

export default function InputForm(props: Props) {
    const { field, type = "text", placeholder = '', className, label, form } = props;
    const { name, value, onChange , onBlur } = field;
    const { errors, touched } = form;

    // have error
    const showError = errors[name] && touched[name];

    // class custom
    const classInput = clsx(styles.inputForm, {
        className: className,
        [styles.showError]: showError
    });
    
    return (
        <Fragment>
             {label && <div className={styles.labelView}>
                    <label className={styles.label} htmlFor={name}>{label}</label>
                </div>}
            <div className={classInput}>
                <input
                    className={styles.inputView}
                    id={name}
                    placeholder={placeholder}
                    type={type}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                />
            </div>
            <div className={styles.errorMess}>
                <ErrorMessage name={name}/>
            </div>
        </Fragment>
    );
}
