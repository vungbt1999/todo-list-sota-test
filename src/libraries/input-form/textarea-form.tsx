import clsx from 'clsx';
import { ErrorMessage } from 'formik';
import { Fragment } from 'react';
import styles from './styles.module.scss';

interface Props {
    field: any;
    form: any;

    type?: any;
    placeholder?: string;
    className?: any;
    label?: string;
    required?: boolean
}

export default function TextAreaForm(props: Props) {
    const { field, placeholder = '', className, label, form } = props;
    const { name, value, onChange , onBlur } = field;
    const { errors, touched } = form;

    // have error
    const showError = errors[name] && touched[name];

    // class custom
    const classTextArea = clsx(styles.textAreaForm, {
        className: className,
        [styles.showError]: showError
    });

    return (
        <Fragment>
            {label && <div className={styles.labelView}>
                    <label className={styles.label} htmlFor={name}>{label}</label>
                </div>}
            <div className={classTextArea}>
                <textarea
                    id={name}
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    className={styles.textAreaView}
                />
            </div>
            <div className={styles.errorMess}>
                <ErrorMessage name={name}/>
            </div>
        </Fragment>
    );
}
