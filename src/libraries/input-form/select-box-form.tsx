import clsx from 'clsx';
import { ErrorMessage } from 'formik';
import { Fragment } from 'react';
import styles from './styles.module.scss';

interface Props {
    field: any;
    form: any;

    placeholder?: string;
    className?: any;
    label?: string;
    required?: boolean;
    options: OptionSelect[];
}

export interface OptionSelect {
    key: string;
    value: any;
}

export default function SelectBoxForm(props: Props) {
    const { field, placeholder = '', options, className, label, form } = props;
    const { name, value, onBlur } = field;
    const { errors, touched, setFieldValue } = form;

    // have error
    const showError = errors[name] && touched[name];

    // class custom
    const classSelect = clsx(styles.selectBoxForm, {
        className: className,
        [styles.showError]: showError
    });

    return (
        <Fragment>
            {label && (
                <div className={styles.labelView}>
                    <label className={styles.label} htmlFor={name}>
                        {label}
                    </label>
                </div>
            )}
            <div className={classSelect}>
                <select 
                    id={name}
                    className={styles.selectBoxView}
                    value={value}
                    placeholder={placeholder}
                    onChange={(e) => setFieldValue(name, Number(e.target.value))}
                    onBlur={onBlur}
                >
                    {options.map((item, index) => {
                        return (
                            <option value={item.value} key={index}>
                                {item.key}
                            </option>
                        );
                    })}
                </select>
            </div>
            <div className={styles.errorMess}>
                <ErrorMessage name={name} />
            </div>
        </Fragment>
    );
}
