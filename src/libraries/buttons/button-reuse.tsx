import styles from './styles.module.scss';
import clsx from 'clsx';

interface Props {
    addBtn?: boolean;
    detailBtn?: boolean;
    removeBtn?: boolean;
    doneBtn?: boolean;
    title: string;
    className?: any;
    onClick?: () => void;
    type?: "button" | "submit" | "reset" | undefined;
};

export default function ButtonReuse({
    addBtn,
    detailBtn,
    removeBtn,
    doneBtn,
    className,
    title,
    type="button",
    onClick
}: Props) {

    const classButton = clsx(styles.buttonReuse, {
        [styles.addBtn]: addBtn,
        [styles.detailBtn]: detailBtn,
        [styles.removeBtn]: removeBtn,
        [styles.doneBtn]: doneBtn,
        className: className
    });

    return (
        <button className={classButton} type={type} onClick={onClick}>
            {title}
        </button>
    )
}
