import NProgress, { NProgressOptions } from 'nprogress';
import 'nprogress/nprogress.css';
import React, { useEffect } from 'react';

interface Props {
    color?: string;
    classCustom?: string;
    children?: React.ReactNode;
    config?: Partial<NProgressOptions>;
}

function LazyLoading(props: Props) {
    const { color, config, children, classCustom } = props;
    const configDefault = {
        showSpinner: false,
        template: `<div class="bar ${classCustom}" 
                style="background:${color || '#1890f'}" role="bar"
                >
                  <div class="peg" 
                  style="box-shadow: 0 0 10px ${color || '#1890f'},
                   0 0 5px ${color || '#1890f'};">
                  </div>
                </div>
                <div class="spinner" role="spinner">
                  <div class="spinner-icon" 
                  style="border-top-color: ${color || '#1890f'};
                   border-left-color: ${color || '#1890f'};">
                  </div>
                </div>`
    };
    useEffect(() => {
        NProgress.configure({ ...configDefault, ...(config || {}) });
        NProgress.start();
        return () => {
            NProgress.done();
        };
    });
    return <>{children}</>;
}

export default LazyLoading;
