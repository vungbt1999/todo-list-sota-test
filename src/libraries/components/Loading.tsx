
interface Props {
    loading?: boolean;
   }

const Loading = (props: Props) => {
    const { loading } = props;
    return (
        <div className="center-element">
            {loading !== false && (
               <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            )}
        </div>
    );
};

export default Loading;
