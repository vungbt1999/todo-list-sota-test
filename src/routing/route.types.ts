import React, { JSXElementConstructor, ReactElement } from 'react';

export interface IRoute {
    path: string;
    element: ReactElement<any, string | JSXElementConstructor<any>> | null | undefined;
    index?: boolean;
    fallback?: React.FC;
}