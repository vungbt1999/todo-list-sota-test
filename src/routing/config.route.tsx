import { IRoute } from './route.types';
import { lazy } from 'react';
const HomePage = lazy(() => import('modules/home/index'));

const configRoutes: IRoute[] = [
    {
        path: '/',
        element: <HomePage />,
    },
];

export default configRoutes;