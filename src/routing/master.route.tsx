import LazyLoading from 'libraries/components/LazyLoading';
import Loading from 'libraries/components/Loading';
import React, { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import { IRoute } from './route.types';

interface PropsRenderRoutes {
    routes: IRoute[];
    loading?: React.ReactNode;
    baseUrl?: string;
}

const MasterRoutes = (props: PropsRenderRoutes) => {
    const { routes, loading, baseUrl } = props;

    return (
        <Suspense
            fallback={
                loading || (
                    <LazyLoading color="#3a44d7">
                        <Loading />
                    </LazyLoading>
                )
            }
        >
            <Routes>
                {routes?.map((route: IRoute, index: number) => {
                    return (
                        <Route
                            key={`${baseUrl || ''}${index}`}
                            path={`${baseUrl || ''}${route.path}`}
                            element={route.element}
                        />
                    );
                })}
            </Routes>
        </Suspense>
    );
};

export default MasterRoutes;
