import { Fragment } from 'react';
import ListTaskComponent from './components/list-task/list-task.component';
import NewTaskComponent from './components/new-task/new-task.component';
import styles from './styles.module.scss';
import HomeUtils from './utils/home.utils';

export default function HomePage() {

    const {
        onAddNewTask,
        onCheckedTask,
        onDeleteTask,
        onSearchTask,
        onViewDetail,
        onSubmitUpdate,
        onRemoveAll,
        dataRender,
        checkedIds,
        idViewDetail
    } = HomeUtils();

    return (
        <Fragment>
            <div className={`main-content ${styles.wrapperHome}`}>
                <NewTaskComponent 
                    onAddNewTask={onAddNewTask}
                />
                <ListTaskComponent 
                    onCheckedTask={onCheckedTask}
                    onDeleteTask={onDeleteTask}
                    onViewDetail={onViewDetail}
                    onSearchTask={onSearchTask}
                    onSubmitUpdate={onSubmitUpdate}
                    onRemoveAll={onRemoveAll}
                    dataRender={dataRender}
                    checkedIds={checkedIds}
                    idViewDetail={idViewDetail}
                />
            </div>
            <div className={styles.footer}>
                <p>@repo<a href="https://gitlab.com/vungbt1999/todo-list-sota-test" target="_blank" rel="noreferrer"> vungbt1999</a></p>
            </div>
        </Fragment>
    )
}
