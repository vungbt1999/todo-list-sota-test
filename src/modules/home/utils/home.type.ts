
export interface Task {
    id: string;
    title: string;
    description?: string;
    duaDate: any;
    priority: PriorityType;
}

export interface TaskInput {
    title: string;
    description?: string;
    duaDate: any;
    priority: PriorityType;
}

export enum PriorityType {
    LOW = 1,
    NORMAL = 2,
    HIGH = 3
}