import { FormikHelpers, FormikProps } from 'formik';
import { Task, TaskInput } from 'modules/home/utils/home.type';
import { useEffect, useState } from 'react';
import { showAlertError, showAlertSuccess } from 'utils/common.utils';

interface Utils {
    onAddNewTask: (values: TaskInput, form: FormikHelpers<any> ) => void;
    onSubmitUpdate: (values: TaskInput) => void;
    onViewDetail: (form: FormikProps<any>, item: Task) => void;
    onDeleteTask: (id: string) => void;
    onCheckedTask: (id: string) => void;
    onSearchTask: (value: string) => void;
    onRemoveAll: () => void;
    dataRender: Task[];
    checkedIds: string[];
    idViewDetail?: string;
}

export default function HomeUtils(): Utils {

    const [listTask, setListTask] = useState<Task[]>([]);

    const [checkedIds, setCheckedIds] = useState<string[]>([]);
    const [idViewDetail, setIdViewDetail] = useState<string>();
    const [keySearch, setKeySearch] = useState<string>();

    const [dataRender, setDataRender] = useState<Task[]>([]);

    useEffect(() => {
        const newListTask = [...listTask];
        if(!keySearch) {
            return setDataRender(newListTask);
        };
        const dataFilter = newListTask.filter(x => x.title.includes(keySearch));
        setDataRender(dataFilter);
    }, [keySearch, listTask]);

    // add new task
    const onAddNewTask = async (values: TaskInput, form: FormikHelpers<any>,) => {
        const newListTask = [...listTask];
        const body: Task = {
            id: String(newListTask.length + 1),
            title: values.title,
            description: values.description,
            duaDate: values.duaDate,
            priority: values.priority
        };
        newListTask.push(body);
        await setListTask(sortDataByDate(newListTask));
        showAlertSuccess('Add task success!');
    };

    // on submit edit
    const onSubmitUpdate = async (values: TaskInput) => {
        const newListTask = [...listTask];
        const indexUpdate = newListTask.findIndex((x) => x.id === idViewDetail);
        const body: Task = {
            ...newListTask[indexUpdate],
            title: values.title,
            description: values.description,
            duaDate: values.duaDate,
            priority: values.priority
        };

        if (indexUpdate === -1) {
            return showAlertError('Task invalid!');
        }
        newListTask.splice(indexUpdate, 1, body);
        await setListTask(sortDataByDate(newListTask));
        showAlertSuccess('Update task success!');
    };

    // on search task
    const onSearchTask = (value: string) => {
        setKeySearch(value);
    };

    // on checked task
    const onCheckedTask = (id: string) => {
        const newCheckedIdS = [...checkedIds];
        const indexInvalid = newCheckedIdS.findIndex((x) => x === id);
        if (indexInvalid !== -1) {
            newCheckedIdS.splice(indexInvalid, 1);
        } else {
            newCheckedIdS.push(id);
        }
        setCheckedIds(newCheckedIdS);
    };

    // on delete task
    const onDeleteTask = async (id: string) => {
        const newListTask = [...listTask];
        const indexValid = newListTask.findIndex((x) => x.id === id);
        if (indexValid !== -1) {
            await newListTask.splice(indexValid, 1);
            setListTask(newListTask);
            return showAlertSuccess('Delete task success!');
        }
        showAlertError('Task invalid!');
    };

    // remove all item checked
    const onRemoveAll = async () => {
        const newListTask = [...listTask];
        const newCheckedIds = [...checkedIds];
        if(newCheckedIds.length <= 0) return showAlertError('Please choose item delete!');
        const dataFilter = newListTask.filter(x => !newCheckedIds.includes(x.id));
        await setListTask(dataFilter);
        setCheckedIds([]);
        return showAlertSuccess('Delete task success!');
    };

    // on view detail
    const onViewDetail = (form: FormikProps<any>, item: Task) => {
        setIdViewDetail(item.id);
        if (idViewDetail === item.id) {
            form.setValues({});
            setIdViewDetail(undefined);
        } else {
            form.setValues({ ...item });
        }
    };

    //sort data
    const sortDataByDate = (data: Task[]): Task[] => {
        return data.sort((a, b) => a.duaDate - b.duaDate);
    }

    return {
        onAddNewTask,
        dataRender,
        checkedIds,
        onCheckedTask,
        onDeleteTask,
        onViewDetail,
        onSearchTask,
        onSubmitUpdate,
        onRemoveAll,
        idViewDetail
    };
}
