import styles from './styles.module.scss';


export default function EmptyData() {
    return (
        <div className={styles.emptyData}>
            <img src="/empty.svg" alt="empty-data"/>
            <p className={styles.title}>Task Empty</p>
        </div>
    )
}
