import ButtonReuse from 'libraries/buttons/button-reuse';
import { Task } from 'modules/home/utils/home.type'
import CheckBoxComponent from './checkbox.component';
import styles from './styles.module.scss';

interface Props {
    onViewDetail: (item: Task) => void;
    onDeleteTask: (id: string) => void;
    onCheckedTask: (id: string) => void;
    item: Task;
    active?: boolean;
};

export default function ItemTaskComponent({
    onViewDetail,
    onCheckedTask,
    onDeleteTask,
    item,
    active
}: Props) {
    return (
        <div className={styles.itemTask}>
            <div className={styles.leftContent}>
                <CheckBoxComponent checked={active} onClick={() => onCheckedTask(item.id)}/>
                <p className={styles.titleTask}>{item.title}</p>
            </div>
            <div className={styles.rightContent}>
                <div className={styles.btnActive}>
                    <ButtonReuse title="Detail" detailBtn onClick={() => onViewDetail(item)}/>
                </div>
                <div className={styles.btnActive}>
                    <ButtonReuse title="Remove" removeBtn onClick={() => onDeleteTask(item.id)}/>
                </div>
            </div>
        </div>
    )
}
