import React from 'react';
import styles from './styles.module.scss';

interface Props {
    checked?: boolean;
    onClick: () => void;
};

export default function CheckBoxComponent({
    checked,
    onClick
}: Props) {
    return (
        <div className={styles.checkBoxView} onClick={onClick}>
            <div className={styles.box}></div>
            {checked && <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor">
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M5 13l4 4L19 7"
                />
            </svg>}
        </div>
    );
}
