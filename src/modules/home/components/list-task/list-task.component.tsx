import { FastField, Form, Formik, FormikProps } from 'formik';
import ButtonReuse from 'libraries/buttons/button-reuse';
import DatePickerForm from 'libraries/input-form/date-picker';
import InputForm from 'libraries/input-form/input-form';
import SelectBoxForm, { OptionSelect } from 'libraries/input-form/select-box-form';
import TextAreaForm from 'libraries/input-form/textarea-form';
import { PriorityType, Task, TaskInput } from 'modules/home/utils/home.type';
import { showAlertError } from 'utils/common.utils';
import * as Yup from 'yup';
import EmptyData from './empty-data.component';
import InputSearchComponent from './input-search.component';
import ItemTaskComponent from './item-task.component';
import styles from './styles.module.scss';

interface Props {
    onViewDetail: (form: FormikProps<any>, item: Task) => void;
    onDeleteTask: (id: string) => void;
    onCheckedTask: (id: string) => void;
    onSearchTask: (value: string) => void;
    onSubmitUpdate: (values: TaskInput) => void;
    dataRender: Task[];
    checkedIds: string[];
    idViewDetail?: string;
    onRemoveAll: () => void;
}

export default function ListTaskComponent({
    onCheckedTask,
    onDeleteTask,
    onSearchTask,
    onViewDetail,
    onSubmitUpdate,
    onRemoveAll,
    checkedIds,
    dataRender,
    idViewDetail
}: Props) {
    const listOptions: OptionSelect[] = [
        {
            key: 'LOW',
            value: PriorityType.LOW
        },
        {
            key: 'NORMAL',
            value: PriorityType.NORMAL
        },
        {
            key: 'HIGH',
            value: PriorityType.HIGH
        }
    ];

    // init form value
    const initialValues: TaskInput = {
        title: '',
        description: undefined,
        duaDate: new Date(),
        priority: PriorityType.NORMAL
    };

    //validation with yup
    const validationSchema = Yup.object().shape({
        title: Yup.string().required('Title task is required'),
        duaDate: Yup.string().required('Dua date is required'),
        priority: Yup.string().required('Priority is required')
    });

    return (
        <div className={styles.wrapperListTask}>
            <p className={styles.title}>To Do List</p>
            <InputSearchComponent onSearch={onSearchTask} placeholder="Search..." />

            {/** List Task */}
            <div className={styles.listTask}>
                {dataRender.length > 0 &&
                    dataRender.map((item, index) => {
                        return (
                            <div key={index} className={styles.wrapperItemTask}>
                                <Formik
                                    initialValues={initialValues}
                                    onSubmit={onSubmitUpdate}
                                    validationSchema={validationSchema}>
                                    {(formikProps) => {
                                        return (
                                            <Form className="text-red">
                                                <ItemTaskComponent
                                                    key={index}
                                                    onCheckedTask={onCheckedTask}
                                                    onDeleteTask={onDeleteTask}
                                                    onViewDetail={(item) =>
                                                        onViewDetail(formikProps, item)
                                                    }
                                                    item={item}
                                                    active={checkedIds.includes(item.id)}
                                                />

                                                {idViewDetail && idViewDetail === item.id && (
                                                    <div className={styles.formDetail}>
                                                        <div className={styles.itemForm}>
                                                            <FastField
                                                                name="title"
                                                                component={InputForm}
                                                                placeholder="Add new task..."
                                                            />
                                                        </div>
                                                        <div className={styles.itemForm}>
                                                            <FastField
                                                                name="description"
                                                                component={TextAreaForm}
                                                                label="Description"
                                                            />
                                                        </div>
                                                        <div className={styles.rowSelect}>
                                                            <div className={styles.itemRowForm}>
                                                                <FastField
                                                                    name="duaDate"
                                                                    component={DatePickerForm}
                                                                    label="Dua Date"
                                                                />
                                                            </div>

                                                            <div className={styles.itemRowForm}>
                                                                <FastField
                                                                    name="priority"
                                                                    component={SelectBoxForm}
                                                                    label="Priority"
                                                                    options={listOptions}
                                                                />
                                                            </div>
                                                        </div>

                                                        <ButtonReuse
                                                            title="Update"
                                                            type="submit"
                                                            addBtn
                                                        />
                                                    </div>
                                                )}
                                            </Form>
                                        );
                                    }}
                                </Formik>
                            </div>
                        );
                    })}
                {dataRender.length <= 0 && <EmptyData />}
            </div>
            {checkedIds.length > 0 && (
                <div className={styles.footerAction}>
                    <div className={styles.leftContent}>
                        <p className={styles.titleFooter}>Bulk Action:</p>
                    </div>
                    <div className={styles.rightContent}>
                        <div className={styles.btnAction}>
                            <ButtonReuse
                                title="Done"
                                doneBtn
                                onClick={() => showAlertError('You not have permission!')}
                            />
                        </div>
                        <div className={styles.btnAction}>
                            <ButtonReuse title="Remove" removeBtn onClick={onRemoveAll} />
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
