import { useDebounceFn } from 'ahooks';
import React, { useEffect, useState } from 'react';
import styles from './styles.module.scss';

interface Props {
    placeholder?: string;
    onSearch: (value: string) => void;
}
export default function InputSearchComponent({
    onSearch,
    placeholder
}: Props) {

    const [search, setSearch] = useState<string>('');
    const { run } = useDebounceFn(
        (value: string) => {
            setSearch(value);
        },
        {
            wait: 600
        }
    );

    useEffect(() => {
        onSearch(search);
    }, [search, onSearch]);

    return (
        <div className={styles.inputSearchView}>
            <input
                className={styles.input} 
                onChange={(e) => run(e.target.value)} 
                placeholder={placeholder}
            />
        </div>
    );
}
