import { FastField, Form, Formik, FormikHelpers } from 'formik';
import ButtonReuse from 'libraries/buttons/button-reuse';
import DatePickerForm from 'libraries/input-form/date-picker';
import InputForm from 'libraries/input-form/input-form';
import SelectBoxForm, { OptionSelect } from 'libraries/input-form/select-box-form';
import TextAreaForm from 'libraries/input-form/textarea-form';
import { PriorityType, TaskInput } from 'modules/home/utils/home.type';
import * as Yup from 'yup';
import styles from './styles.module.scss';

interface Props {
    onAddNewTask: (values: TaskInput, form: FormikHelpers<any>) => void;
};
export default function NewTaskComponent({
    onAddNewTask
}: Props) {

    const listOptions: OptionSelect[] = [
        {
            key: 'LOW',
            value: PriorityType.LOW
        },
        {
            key: 'NORMAL',
            value: PriorityType.NORMAL
        },
        {
            key: 'HIGH',
            value: PriorityType.HIGH
        },
    ];
    
    // init form value
    const initialValues: TaskInput = {
        title: '',
        description: undefined,
        duaDate: new Date(),
        priority: PriorityType.NORMAL
    };

    //validation with yup
    const validationSchema = Yup.object().shape({
        title: Yup.string().required('Title task is required'),
        duaDate: Yup.string().required('Dua date is required'),
        priority: Yup.string().required('Priority is required')
    });

    return (
        <div className={styles.wrapperNewTask}>
            <p className={styles.title}>New Task</p>
            <div className={styles.formContent}>
                <Formik
                    initialValues={initialValues}
                    onSubmit={onAddNewTask}
                    validationSchema={validationSchema}>
                    {(formikProps) => {
                        return (
                            <Form className="text-red">
                                <div className={styles.itemForm}>
                                    <FastField
                                        name="title"
                                        component={InputForm}
                                        placeholder="Add new task..."
                                    />
                                </div>
                                <div className={styles.itemForm}>
                                    <FastField
                                        name="description"
                                        component={TextAreaForm}
                                        label="Description"
                                    />
                                </div>
                                <div className={styles.rowSelect}>
                                    <div className={styles.itemRowForm}>
                                        <FastField
                                            name="duaDate"
                                            component={DatePickerForm}
                                            label="Dua Date"
                                        />
                                    </div>

                                    <div className={styles.itemRowForm}>
                                        <FastField
                                            name="priority"
                                            component={SelectBoxForm}
                                            label="Priority"
                                            options={listOptions}
                                        />
                                    </div>
                                </div>
                               
                                <ButtonReuse title="Add" type="submit" addBtn/>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        </div>
    );
}
