import { toast } from 'react-toastify';
import { ToastSetting } from 'constants/common';

export const showLoading = (content: string, key?: string): void => {
    toast.loading(content, ToastSetting);
};

export const showAlertSuccess = (content: string): void => {
    toast.success(content, ToastSetting);
};

export const showAlertError = (content: string): void => {
    toast.error(content, ToastSetting);
};
