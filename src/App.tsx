import { Fragment } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import configRoutes from 'routing/config.route';
import MasterRoutes from 'routing/master.route';
import 'react-toastify/dist/ReactToastify.css';
import 'styles/global.scss';
import { ToastContainer } from 'react-toastify';


function App() {
    return (
        <Fragment>
            <Router>
                <MasterRoutes routes={configRoutes}/>
            </Router>
            <ToastContainer />
        </Fragment>
    );
}

export default App;
